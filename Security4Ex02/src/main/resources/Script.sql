DROP TABLE users;
CREATE  TABLE users (
  username VARCHAR2(100) NOT NULL ,
  password VARCHAR2(100) NOT NULL ,
  enabled  NUMBER DEFAULT 1 ,
  PRIMARY KEY (username));
 
DROP SEQUENCE  user_roles_idx_SEQ;
CREATE SEQUENCE user_roles_idx_SEQ;
DROP TABLE user_roles;
CREATE TABLE user_roles (
  user_role_id number NOT NULL,
  username varchar(45) NOT NULL,
  role varchar(45) NOT NULL,
  PRIMARY KEY (user_role_id));
 
INSERT INTO users(username,password,enabled)
VALUES ('kimc','1234', 1);
INSERT INTO users(username,password,enabled)
VALUES ('leec','1234', 1);
INSERT INTO users(username,password,enabled)
VALUES ('hongc','1234', 1);

INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'kimc', 'ROLE_USER');
INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'leec', 'ROLE_USER');
INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'leec', 'ROLE_DBA');
INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'hongc', 'ROLE_USER');
INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'hongc', 'ROLE_ADMIN');
INSERT INTO user_roles (user_role_id,username, role)
VALUES (user_roles_idx_SEQ.NEXTVAL,'hongc', 'ROLE_DBA');

SELECT * FROM users;
SELECT * FROM USER_ROLES;
COMMIT;


 