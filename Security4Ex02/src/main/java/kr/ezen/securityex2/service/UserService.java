package kr.ezen.securityex2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.ezen.securityex2.dao.UserDAO;

@Service
public class UserService {
	@Autowired
	private UserDAO userDAO;
	
	public String now() {
		return userDAO.now();
	}
}
